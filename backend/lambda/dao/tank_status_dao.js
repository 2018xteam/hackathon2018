const DynamoDB = require('aws-sdk').DynamoDB;
const db = new DynamoDB.DocumentClient();

const TableName = "HACK_TANK_STATUS";

module.exports.getItemByBuildingid = (tankid) =>
{
    return db.get(
    {
        TableName,
        Key:
        {
            tankid
        }
    }).promise();
};