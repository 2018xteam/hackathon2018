const DynamoDB = require('aws-sdk').DynamoDB;
const db = new DynamoDB.DocumentClient();

const TableName = "HACK_BUILDING";

module.exports.getItemByBuildingid = (buildingid) =>
{
    return db.get(
    {
        TableName,
        Key:
        {
            buildingid
        }
    }).promise();
};

module.exports.scanItem = () =>
{
    return db.scan(
    {
        TableName,
    }).promise();
};