const DynamoDB =  require('aws-sdk').DynamoDB;
const db = new DynamoDB.DocumentClient();

const TableName = "HACK_TANK_HISTORY";

module.exports.queryBytankid = (tankid) =>
{
    return db.query(
    {
        TableName,
        KeyConditionExpression: "tankid = :tankid",
        // KeyConditionExpression: "tankid = :tankid and tstamp > :value",
        ExpressionAttributeValues:
        {
            ':tankid': tankid,
            // ':value': Number(Date.now() - 60 * 60 * 24 * 1000)
        }
    }).promise();
};{
    
}