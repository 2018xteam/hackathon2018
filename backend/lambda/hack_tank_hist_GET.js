let building_dao = require('./dao/building_dao')
let tank_hist_dao = require('./dao/tank_hist_dao');
let tank_status_dao = require('./dao/tank_status_dao');

exports.handler = async (event, context, callback) =>
{
    let tankid = event.queryStringParameters.tankid;
    console.log('tankid', tankid);
    let tank_status_get_res = await tank_hist_dao.queryBytankid(tankid);
    if(tank_status_get_res.Count == 0)
    {
        callback(null,
            {
                statusCode: 200,
                headers: null,
                body: JSON.stringify([])
            });
    }
    let res = tank_status_get_res.Items;
    
    console.log(JSON.stringify(res, null, 2));
    callback(null,
    {
        statusCode: 200,
        headers: null,
        body: JSON.stringify(res)
    });
};
