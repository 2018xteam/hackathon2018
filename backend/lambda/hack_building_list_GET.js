let building_dao = require('./dao/building_dao')
let tank_hist_dao = require('./dao/tank_hist_dao');
let tank_status_dao = require('./dao/tank_status_dao');

exports.handler = async (event, context, callback) =>
{
    let building_get_res = await building_dao.scanItem();
    let res = building_get_res.Items;
    // console.log(JSON.stringify(res, null, 2));
    if(!res.Count == 0)
    {
        callback(null,
            {
                statusCode: 200,
                headers: null,
                body: JSON.stringify(null)
            });
    }
    let result = []
    
    for(let i = 0; i<res.length; i++)
    {
        let singleBuilding = res[i];
        let tanks = [];
        for(let j = 0; j<singleBuilding.tanks.length; j++)
        {
            let tankid = singleBuilding.tanks[j].tankid;
            // console.log("tankid",tankid);
            let hist_res = await tank_hist_dao.queryBytankid(tankid);
            let single_res = await tank_status_dao.getItemByBuildingid(tankid);
            // {
            //     "waterlevel": 148,
            //     "tstamp": 1538745867581,
            //     "tankid": "tid-acedd0e1b1b21ccc37eac5ea863bc93e",
            //     "buildingid": "tid-acedd0e1b1b21ccc37eac5ea863bc93e",
            //     "waterquality": 98,
            //     "motor": false,
            //     "floor": -1
            // }

            let id = single_res.Item.tankid;
            let floor = single_res.Item.floor;
            let motor = single_res.Item.motor;
            let records = [];
            for (let k = 0; k < hist_res.Items.length; k++)
            {
                let single_hist = hist_res.Items[k];
                records.push({
                    waterlevel: single_hist.waterlevel,
                    tstamp: single_hist.tstamp,
                    waterquality: single_hist.waterquality,
                    motor: single_hist.motor
                });
            }
            // console.log('id', id)
            // console.log({
            //     id,
            //     floor,
            //     motor,
            //     // records
            // })
            tanks.push({
                id,
                floor,
                motor,
                records
            })
        }
        // if(i == 0)
        // {
        //     console.log("tanks", JSON.stringify(tanks,null,2));
        // }
        res[i].tanks = tanks;
    }
    // console.log(JSON.stringify(res, null, 2));
    callback(null,
    {
        statusCode: 200,
        headers: null,
        body: JSON.stringify(res)
    });
};

exports.handler();