let building_dao = require('./dao/building_dao')
let tank_hist_dao = require('./dao/tank_hist_dao');
let tank_status_dao = require('./dao/tank_status_dao');

exports.handler = async (event, context, callback) =>
{
    let buildingid = event.queryStringParameters.buildingid;
    console.log('buildingid', buildingid);
    if(!buildingid)
    {
        callback(null,
            {
                statusCode: 200,
                headers: null,
                body: JSON.stringify(null)
            });
    }
    let building_get_res = await building_dao.getItemByBuildingid(buildingid);
    let res = building_get_res.Item;
    console.log(JSON.stringify(res, null, 2));
    if(!res)
    {
        callback(null,
            {
                statusCode: 200,
                headers: null,
                body: JSON.stringify(null)
            });
    }

    let result = {}
    for(let i = 0; i<res.tanks.length; i++)
    {
        let item = res.tanks[i];
        let tankid = item.tankid;
        let res1 = await tank_hist_dao.queryBytankid(tankid);
        console.log('res1', JSON.stringify(res1, null, 2))
        if(res1.Count !== 0)
        {
            result[tankid] = res1.Items || [];
        }
        
        // res.tanks[i] = res1.Items || [];
    }
    res.tanks = result;
    console.log(JSON.stringify(res, null, 2));
    callback(null,
    {
        statusCode: 200,
        headers: null,
        body: JSON.stringify(res)
    });
};