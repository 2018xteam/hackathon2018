
package hackathon2018;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;

import com.amazonaws.services.dynamodbv2.AmazonDynamoDBClientBuilder;
import com.amazonaws.services.dynamodbv2.document.DynamoDB;
import com.amazonaws.services.dynamodbv2.document.Item;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;

public class App
{
	private static final int[] LEVEL = { 172, 178, 184, 186, 186, 186, 183, 178, 177, 176, 172, 169, 169, 169, 169, 169, 169, 164, 160, 158, 151, 147, 143, 165 };
	private static final int[] QUALITY = { 98, 98, 98, 98, 98, 98, 97, 97, 97, 97, 97, 96, 96, 96, 96, 96, 97, 97, 97, 97, 98, 98, 98, 98 };
	private static final boolean[] ONOFF = { true, true, true, true, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, true, true, true, true };

	static final Gson GSON = new GsonBuilder().create();// .setPrettyPrinting().create();

	private static DynamoDB ddb = new DynamoDB(AmazonDynamoDBClientBuilder.standard().build());

	public static void main(String... strings) throws IOException
	{
		String content = new String(Files.readAllBytes(Paths.get("./buildings.json")));
		JsonArray json = GSON.fromJson(content, JsonArray.class);
		json.forEach(o ->
		{
			insertBuilding(o.getAsJsonObject());
		});
	}

	public static void insertBuilding(JsonObject json)
	{
		System.out.println(json);
		String bid = json.get("buildingid").getAsString();
		ddb.getTable("HACK_BUILDING").putItem(Item.fromJSON(json.toString()));
		JsonArray tanks = json.getAsJsonArray("tanks");
		tanks.forEach(o ->
		{
			generateTankData(bid, o.getAsJsonObject().get("tankid").getAsString(), o.getAsJsonObject().get("floor").getAsInt());
		});
	}

	public static void generateTankData(String buildingid, String tankid, int floor)
	{
		if ( "tid-9cc32bb529694d0721043ca77012431d".equals(tankid) )
		{
			for (int i = 240; i > 0; i--)
			{
				Instant now = Instant.now().minusSeconds(i * 3600);
				LocalDateTime time = LocalDateTime.ofInstant(now, ZoneId.of("UTC+8"));
				int hour = time.getHour();
				TankStatus ts;
				if ( i < 72 )
				{
					int idx = hour + (i / 24) * 24;
					ts = new TankStatus(tankid, now.toEpochMilli(), buildingid, floor,
									FAIL[i],
									96,
									false);
					System.out.println("FAIL i=" + i + " idx:" + idx + " json:" + ts.toJsonString());
				}
				else
				{
					boolean trend = ONOFF[hour];
					ts = new TankStatus(tankid, now.toEpochMilli(), buildingid, floor,
									trend? LEVEL[hour] + (int) (Math.random() * 3):LEVEL[hour] - (int) (Math.random() * 3),
									QUALITY[hour] + (int) (Math.random() * 3) - 2,
									trend);
					System.out.println("WORK i=" + i + " hour:" + hour + " json:" + ts.toJsonString());
				}
				Item item = Item.fromJSON(ts.toJsonString());
				ddb.getTable("HACK_TANK_HISTORY").putItem(item);
			}
			Instant now = Instant.now();
			TankStatus ts = new TankStatus(tankid, now.toEpochMilli(), buildingid, floor,
							FAIL[0],
							96,
							false);
			System.out.println(now + ":" + ts.toJsonString());
			Item item = Item.fromJSON(ts.toJsonString());
			ddb.getTable("HACK_TANK_STATUS").putItem(item);
			ddb.getTable("HACK_TANK_HISTORY").putItem(item);
		}
		else
		{
			boolean trend;
			for (int i = 240; i > 0; i--)
			{
				Instant now = Instant.now().minusSeconds(i * 3600);
				LocalDateTime time = LocalDateTime.ofInstant(now, ZoneId.of("UTC+8"));
				int hour = time.getHour();
				trend = ONOFF[hour];
				TankStatus ts = new TankStatus(tankid, now.toEpochMilli(), buildingid, floor,
								trend? LEVEL[hour] + (int) (Math.random() * 3):LEVEL[hour] - (int) (Math.random() * 3),
								QUALITY[hour] + (int) (Math.random() * 3) - 2,
								trend);
				System.out.println(now + ":" + ts.toJsonString());
				Item item = Item.fromJSON(ts.toJsonString());
				ddb.getTable("HACK_TANK_HISTORY").putItem(item);
			}
			Instant now = Instant.now();
			LocalDateTime time = LocalDateTime.ofInstant(now, ZoneId.of("UTC+8"));
			int hour = time.getHour();
			trend = ONOFF[hour];
			TankStatus ts = new TankStatus(tankid, now.toEpochMilli(), buildingid, floor,
							trend? LEVEL[hour] + (int) (Math.random() * 3):LEVEL[hour] - (int) (Math.random() * 3),
							QUALITY[hour] + (int) (Math.random() * 3) - 2,
							trend);
			System.out.println(now + ":" + ts.toJsonString());
			Item item = Item.fromJSON(ts.toJsonString());
			ddb.getTable("HACK_TANK_STATUS").putItem(item);
			ddb.getTable("HACK_TANK_HISTORY").putItem(item);
		}
	}

	private static final int[] FAIL = { 72,
					74,
					76,
					78,
					81,
					83,
					85,
					87,
					88,
					89,
					91,
					93,
					95,
					98,
					102,
					103,
					106,
					109,
					112,
					112,
					112,
					112,
					112,
					113,
					115,
					117,
					119,
					121,
					124,
					126,
					128,
					130,
					131,
					132,
					134,
					136,
					138,
					141,
					145,
					146,
					149,
					152,
					152,
					154,
					155,
					155,
					155,
					155,
					155,
					155,
					156,
					156,
					157,
					158,
					159,
					161,
					162,
					163,
					165,
					167,
					169,
					169,
					172,
					176,
					177,
					178,
					180,
					183,
					184,
					186,
					186,
					186 };
}
