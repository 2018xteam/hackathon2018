/*
 * TankStatus.java
 * Copyright 2018 thomas@acrosome.com
 */

package hackathon2018;

import java.io.Serializable;

public class TankStatus implements Serializable
{
	private static final long serialVersionUID = -2194108987671966038L;

	private String tankid;
	private long tstamp;
	private String buildingid;
	private int floor;
	private int waterlevel; // cm
	private int waterquality; // 0~100
	private boolean motor; // on|off

	public TankStatus(String tankid, long tstamp, String buildingid, int floor, int waterlevel, int waterquality, boolean motor)
	{
		this.tankid = tankid;
		this.tstamp = tstamp;
		this.buildingid = buildingid;
		this.floor = floor;
		this.waterlevel = waterlevel;
		this.waterquality = waterquality;
		this.motor = motor;
	}

	public static TankStatus fromJsonString(String json)
	{
		return App.GSON.fromJson(json, TankStatus.class);
	}

	public String toJsonString()
	{
		return App.GSON.toJson(this);
	}

	public long getTstamp()
	{
		return tstamp;
	}

	public void setTstamp(long tstamp)
	{
		this.tstamp = tstamp;
	}

	public String getTankid()
	{
		return tankid;
	}

	public String getDeviceid()
	{
		return tankid;
	}

	public void setTankid(String deviceid)
	{
		this.tankid = deviceid;
	}

	public String getBuildingid()
	{
		return buildingid;
	}

	public void setBuildingid(String buildingid)
	{
		this.buildingid = buildingid;
	}

	public int getFloor()
	{
		return floor;
	}

	public void setFloor(int floor)
	{
		this.floor = floor;
	}

	public int getWaterlevel()
	{
		return waterlevel;
	}

	public void setWaterlevel(int waterlevel)
	{
		this.waterlevel = waterlevel;
	}

	public int getWaterquality()
	{
		return waterquality;
	}

	public void setWaterquality(int waterquality)
	{
		this.waterquality = waterquality;
	}

	public boolean isMotor()
	{
		return motor;
	}

	public void setMotor(boolean motor)
	{
		this.motor = motor;
	}

	@Override
	public String toString()
	{
		StringBuilder builder = new StringBuilder();
		builder.append("TankStatus [tankid=");
		builder.append(tankid);
		builder.append(", tstamp=");
		builder.append(tstamp);
		builder.append(", buildingid=");
		builder.append(buildingid);
		builder.append(", floor=");
		builder.append(floor);
		builder.append(", waterlevel=");
		builder.append(waterlevel);
		builder.append(", waterquality=");
		builder.append(waterquality);
		builder.append(", motor=");
		builder.append(motor);
		builder.append("]");
		return builder.toString();
	}
};